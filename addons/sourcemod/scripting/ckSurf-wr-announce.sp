#pragma semicolon 1

#define PLUGIN_AUTHOR "Spectate"
#define PLUGIN_VERSION "1.01"

// Chat colors
#define WHITE 0x01
#define DARKRED 0x02
#define MOSSGREEN 0x05
#define LIMEGREEN 0x06
#define RED 0x07
#define GRAY 0x08
#define DARKBLUE 0x0C

#define WR_RELATIVE_SOUND_PATH "*quake/holyshit.mp3"
#define WR_FULL_SOUND_PATH "sound/quake/holyshit.mp3"

#include <sourcemod>
#include <sdktools>
#include <cstrike>

#pragma newdecls required


float _mapRecord;

Handle g_hDb = null;

char _latestRecord[] = "SELECT steamid,name,runtime,map FROM ck_latestrecords ORDER BY date DESC LIMIT 1";


public Plugin myinfo = 
{
	name = "ckSurf sr announce", 
	author = PLUGIN_AUTHOR, 
	description = "Announces Serverrecords on other servers", 
	version = PLUGIN_VERSION, 
	url = ""
};

public void OnPluginStart()
{
	connectToDB();
	
	LoadTranslations("wrannounce.phrases");
}

public void OnMapStart()
{
	CreateTimer(15.0, TIMER_GetMapRecord);
	LoadTranslations("wrannounce.phrases");
	
	AddFileToDownloadsTable(WR_FULL_SOUND_PATH);
	FakePrecacheSound(WR_RELATIVE_SOUND_PATH);
}

public Action TIMER_GetMapRecord(Handle timer)
{
	db_getLatestRecord(true);
	CreateTimer(15.0, TIMER_CheckForNewRecord, _, TIMER_REPEAT);
}

public Action TIMER_CheckForNewRecord(Handle timer)
{
	db_getLatestRecord(false);
}

public void connectToDB()
{
	char szError[255];
	g_hDb = SQL_Connect("cksurf", false, szError, 255);
	
	if (g_hDb == null)
	{
		SetFailState("[ckSurf-Announce] Unable to connect to database (%s)", szError);
		return;
	}
}

public void db_getLatestRecord(bool first)
{
	SQL_TQuery(g_hDb, sql_selectLatestRecordsCallback, _latestRecord, first, DBPrio_Low);
}

public void sql_selectLatestRecordsCallback(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == null)
	{
		LogError("[ckSurf-Announce] SQL Error (sql_selectLatestRecordsCallback): %s", error);
		return;
	}
	
	char szName[64];
	char szMapName[64];
	char szSteamId[64];
	char szTime[32];
	int time;
	int record;
	float ftime;
	bool writeMessage = true;
	
	if (SQL_HasResultSet(hndl))
	{
		while (SQL_FetchRow(hndl))
		{
			SQL_FetchString(hndl, 0, szSteamId, 64);
			SQL_FetchString(hndl, 1, szName, 64);
			ftime = SQL_FetchFloat(hndl, 2);
			SQL_FetchString(hndl, 3, szMapName, 64);
			
			time = RoundFloat(ftime * 10000);
			record = RoundFloat(_mapRecord * 10000);
			
			if (record != time && !data)
			{
				for (int i = 1; i <= MaxClients; i++)
				{
					if (IsClientInGame(i))
					{
						char clientAuth[32] = "";
						GetClientAuthId(i, AuthId_Steam2, clientAuth, 32, true);
						if (StrEqual(clientAuth, szSteamId))
						{
							writeMessage = false;
							break;
						}
					}
				}
				
				if (writeMessage)
				{
					FormatTimeFloat(data, ftime, szTime, sizeof(szTime));
					EmitSoundToAll(WR_RELATIVE_SOUND_PATH, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL, -1, NULL_VECTOR, NULL_VECTOR, true, 0.0);
					PrintToChatAll("%t", "NewMapRecord", MOSSGREEN, WHITE, LIMEGREEN, szName, GRAY, DARKBLUE, GRAY, RED, szMapName, GRAY, RED, szTime);
				}
				_mapRecord = ftime;
			}
			else if (data)
			{
				_mapRecord = SQL_FetchFloat(hndl, 2);
			}
		}
	}
}


//Credit to jonitaikaponi
public void FormatTimeFloat(int client, float time, char[] string, int length)
{
	char szMilli[16];
	char szSeconds[16];
	char szMinutes[16];
	char szHours[16];
	int imilli;
	int iseconds;
	int iminutes;
	int ihours;
	time = FloatAbs(time);
	imilli = RoundToZero(time * 100);
	imilli = imilli % 100;
	iseconds = RoundToZero(time);
	iseconds = iseconds % 60;
	iminutes = RoundToZero(time / 60);
	iminutes = iminutes % 60;
	ihours = RoundToZero((time / 60) / 60);
	
	if (imilli < 10)
		Format(szMilli, 16, "0%d", imilli);
	else
		Format(szMilli, 16, "%d", imilli);
	if (iseconds < 10)
		Format(szSeconds, 16, "0%d", iseconds);
	else
		Format(szSeconds, 16, "%d", iseconds);
	if (iminutes < 10)
		Format(szMinutes, 16, "0%d", iminutes);
	else
		Format(szMinutes, 16, "%d", iminutes);
	if (ihours > 0)
	{
		Format(szHours, 16, "%d", ihours);
		Format(string, length, "%s:%s:%s:%s", szHours, szMinutes, szSeconds, szMilli);
	}
	else
		Format(string, length, "%s:%s:%s", szMinutes, szSeconds, szMilli);
}

stock void FakePrecacheSound(const char[] szPath)
{
	AddToStringTable(FindStringTable("soundprecache"), szPath);
} 